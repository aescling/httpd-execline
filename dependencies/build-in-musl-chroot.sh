#!/bin/sh -x
# install a usable musl-linked chroot and run the build in there (linux-only!)
# intall the built rutime dependencies into the httpd.execline directory
#
# expected to run in this directory
# builds the result into `_build` in this directiory

fail() {
	echo "${0}: fatal: ${@}" 1>&2
	exit 1
}

usage() {
	fail "usage: HTTPD_EXECLINE_ROOT=<chroot jail directory> ${0}"
}

[ x"${HTTPD_EXECLINE_ROOT}" = x ] && usage
[ ! -r "${HTTPD_EXECLINE_ROOT}" ] && fail "HTTPD_EXECLINE_ROOT (${HTTPD_EXECLINE_ROOT}) is unreadable"

export _CHROOT_BUILD="true"
ARCH=${1:-x86_64}
if [ "x${ARCH}" = aarch64 ]
then
	XBPS_REPOSITORY=https://repo-default.voidlinux.org/current/aarch64
else
	XBPS_REPOSITORY=https://repo-default.voidlinux.org/current/musl
fi
XBPS_STATIC="https://repo-default.voidlinux.org/static/xbps-static-latest.${ARCH}-musl.tar.xz"
CHROOT=xbps/_root

# prepare chroot
for D in /var/db/xbps/keys /etc /proc /sys /dev /run;
do
	mkdir -p "${CHROOT}"${D}
done
cp -Lr /etc/hosts /etc/resolv.conf "${CHROOT}"/etc
cp -r build.sh _9base _toybox "${CHROOT}"

# install into chroot
(
	cd xbps
	curl -Ss "${XBPS_STATIC}" | unxz -c | tar -xf -
)
XBPS_ARCH=${ARCH}-musl ./xbps/usr/bin/xbps-install -y -S -r "${CHROOT}" \
       	-R ${XBPS_REPOSITORY} \
		base-voidstrap base-devel git

unmount() {
	for D in /proc /sys /dev /run
	do
		umount "${CHROOT}"${D}
	done
}

# chroot

# TODO: this behavior is strange
trap unmount INT TERM EXIT

mount -t proc none "${CHROOT}"/proc
mount -t sysfs none "${CHROOT}"/sys
mount --rbind /dev "${CHROOT}"/dev
mount --rbind /run "${CHROOT}"/run

chroot "${CHROOT}" /build.sh

unmount

# intsall
cp -fv $(find -type f "${CHROOT}"/_build/bin) "${HTTPD_EXECLINE_ROOT}"/binaries